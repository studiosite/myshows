<?php

namespace studiosite\myshows\helpers;

/**
 * Валидаторы
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 *
 */
class Validator
{
	/**
	* Валидатор хоста
	* @param string Хост
	* @return bool Валидность
	*/
	public function host($host)
	{
		if (!\parse_url($host))
			return false;

		return true;
	}
}
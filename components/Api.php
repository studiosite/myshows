<?php
namespace studiosite\myshows\components;

use Exception;
use GuzzleHttp\Client;
use InvalidArgumentException;

use studiosite\myshows\helpers\Validator;

/**
 * Компонент апи абстрактный для выгрузки
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 *
 */
class Api
{
    /**
     * @var string Формат запроса по умолчанию
     */
    public $defaultTypeRequest = 'GET';

    /**
     * @var string Формат ответа (xml|json)
     * @todo Поддержка других форматов, кроме json
     */
    // public $format = 'json';

    /**
     * @var string Адрес сервера
     */
    public $host = 'https://api.myshows.me';

    /**
     * @var object instanceof Browser
     */
    private $_browser;

    /**
     * Конструктор компонента
     * @param string $host
     */
    public function __construct($properties = [])
    {
        foreach ($properties as $name => $value) {
            $this->$name = $value;
        }

        $this->init();
    }

    /**
     * Получить сериал по id
     * @param integer $id
     * @return []
     */
    public function getMovieById($id)
    {
        $response = $this->query('shows/'.intval($id));

        if (!$response) {
            return false;
        }

        return $response;
    }

    /**
     * Поиск сериала
     * @param string $term
     * @return []
     */
    public function getMoviesByTerm($term)
    {
        $response = $this->query('shows/search/', [
            'q' => $term,
        ]);

        if (!$response) {
            return [];
        }

        return $response;
    }

    /**
     * Инициализация компонента
     * Инициализация браузера
     */
    public function init()
    {
        $this->_browser = new Client();

        if (!Validator::host($this->host)) {
            throw new InvalidArgumentException('Invalid specified host');
        }
    }

    /**
     * Публичная функция запроса
     * @param string Пространство для запроса
     * @param array Параметры
     * @param string Тип запроса (GET|POST|...)
     */
    public function query(
        $requestNameSpace,
        $params = [],
        $typeRequest = false
    ) {
        $params = $this->setParams($params);

        return $this->_query($requestNameSpace, $params, $typeRequest);
    }

    /**
     * Получить обект парсера ответа
     *
     * @return \studiosite\cinemaccapi\interfaces\Parser
     */
    public function resolveTypeResponse($header)
    {
        $matches = [];

        if (preg_match('/[a-zA-Z0-9\\-]+\\/([a-zA-Z0-9\\-]+)/i', mb_strtolower(implode(";", $header)), $matches)) {
            $format = $matches[1];

            if ($format) {
                $parseClassName = '\\studiosite\\cinemaccapi\\parsers\\'.ucfirst($format);

                if (!class_exists($parseClassName)) {
                    throw new InvalidArgumentException('No support for the chosen format "'.$format.'"');
                }

                return (new $parseClassName());
            }
        }

        return false;
    }

    /**
     * Установка параметров для авторизации
     * @param array Параметры
     */
    public function setParams($params = [])
    {
        return array_merge($params, [
            //'apikey' => $this->apiKey,
            //'format' => $this->format,
        ]);
    }

    /**
     * Общий варинт запроса
     * @param string Пространство для запроса
     * @param array Параметры
     * @param string Тип запроса (GET|POST|...)
     */
    private function _query(
        $requestNameSpace,
        $params,
        $typeRequest = false
    ) {
        $typeRequest = $typeRequest ?: $this->defaultTypeRequest;
        $url = $this->host."/".$requestNameSpace;

        $response = $this->_browser->request($typeRequest, $url, [
            'query' => $params,
        ]);

        $responseCode = $response->getStatusCode();

        if ($responseCode !== 200) {
            throw new Exception('Invalid response code - '.$responseCode);
        }

        return json_decode($response->getBody());
    }
}

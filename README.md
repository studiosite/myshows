# MyShows API

Компонент для работы с [myshows.me](https://myshows.me/)  [API](http://api.myshows.me).

Частичная реализация. Только поиск сериалов и подробности

## Установка

Предпочтительный способ установить это расширение через композитор. [composer](http://getcomposer.org/download/).

Пакет приватный, установка потребует прописать репозиторий с bitbucket.

Необходимо добавить

```
"studiosite/myshows": "dev-master"
```

в секции ```require``` `composer.json` файла.

В секции ```repositories``` добавить

```
{
    "type": "vcs",
    "url": "https://bitbucket.org/studiosite/myshows"
}
```

## Использование

```php
$api = new \studiosite\myshows\components\Api();
$r = $api->getMoviesByTerm('санта-барбара');
var_dump($r);
$r = $api->getMoviesByid(113);
var_dump($r);
```

